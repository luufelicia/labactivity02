package hellopackage;
// import java.util.Scanner;
// import java.util.Random;
import java.util.*;
import secondpackage.Utilities;


public class Greeter
{
    public static void main(String[] args)
    {   
        Scanner scan = new Scanner(System.in);
        // String foo = scan.next();
        // Random rand = new Random();

        System.out.println("Please enter a number.");
        int userNum = scan.nextInt();

        Utilities utility = new Utilities();
        int doubled = utility.doubleMe(userNum);
        System.out.println(doubled);
    }


}